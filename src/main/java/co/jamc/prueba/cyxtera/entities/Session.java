package co.jamc.prueba.cyxtera.entities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Entidad que modela la sesión del espacio de trabajo (workspace).")
@Document("Session")
public class Session {
	
	@ApiModelProperty(name = "sessionID", notes = "Identificador de la sesión generado mediante un componente UUID.")
	@Id
	private UUID sessionID;
	@ApiModelProperty(name = "username", notes = "Nombre de usuario enviado al crear la sesión.")
	@Indexed(name = "usename")
	private String user;
	
	@ApiModelProperty(name = "listaOperandos", notes = "Lista con los operandos ordenada según su secuencia de adición.")
	private List<BigDecimal> listaOperandos = new ArrayList<BigDecimal>();

	public UUID getSessionID() {
		return sessionID;
	}

	public void setSessionID(UUID sessionID) {
		this.sessionID = sessionID;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public List<BigDecimal> getListaOperandos() {
		return listaOperandos;
	}

	public void setListaOperandos(List<BigDecimal> listaOperandos) {
		this.listaOperandos = listaOperandos;
	}

}
