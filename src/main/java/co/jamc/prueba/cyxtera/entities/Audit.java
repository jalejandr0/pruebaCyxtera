package co.jamc.prueba.cyxtera.entities;


import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Entidad que modela los registros de auditoría de la solución.")
@Document("Audit")
public class Audit {
	
	@ApiModelProperty(name = "id", notes = "Componente OjectId que almacena el identificador del registro. Está compuesto por 12 bytes divididos: timestamp - machineIdentifier - processIdentifier - counter")
	@Id
    private ObjectId id;
	
	@ApiModelProperty(name = "sessionId", notes = "Identificador de la sesión generado mediante un componente UUID.")
	@Indexed(name = "session_id")
    private String sessionId;

	@ApiModelProperty(name = "apiInvoke", notes = "Path del API inocado que generó el registro de auditoría.")
    @Indexed(name = "api_invoked")
    private String apiInvoked;
    
	@ApiModelProperty(name = "username", notes = "Nombre de usuario enviado al crear la sesión.")
    @Indexed(name = "user")
    private String user;
	
	@ApiModelProperty(name = "input", notes = "Datos o mensaje de entrada.")
    private String input;

	@ApiModelProperty(name = "output", notes = "Datos o mensaje de salida.")
    private String output;
    
	@ApiModelProperty(name = "dateInv", notes = "Fecha de registro de la acción en la auditoría.")
    @Indexed(name = "dateInv")
    private Date dateInv;

    public Audit() {
    	id = ObjectId.get();
    }

    public Audit(String sessionId, String user, String apiInvoked, String input, String output, Date dateInv) {
    	this();
        this.sessionId = sessionId;
        this.user = user;
        this.apiInvoked = apiInvoked;
        this.input = input;
        this.output = output;
        this.dateInv = dateInv;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getApiInvoked() {
        return apiInvoked;
    }

    public void setApiInvoked(String apiInvoked) {
        this.apiInvoked = apiInvoked;
    }

    public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public Date getDateInv() {
        return dateInv;
    }

    public void setDateInv(Date dateInv) {
        this.dateInv = dateInv;
    }
}
