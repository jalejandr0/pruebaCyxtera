package co.jamc.prueba.cyxtera.persistence;


import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import co.jamc.prueba.cyxtera.entities.Audit;

@Repository
public interface AuditRepository extends MongoRepository<Audit, ObjectId> {

    List<Audit> findBySessionId(String sessionId);
    
    List<Audit> findByUser(String user);

}
