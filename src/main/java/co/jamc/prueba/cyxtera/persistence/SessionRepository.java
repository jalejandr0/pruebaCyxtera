package co.jamc.prueba.cyxtera.persistence;

import java.util.UUID;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import co.jamc.prueba.cyxtera.entities.Session;

@Repository
public interface SessionRepository extends MongoRepository<Session, UUID> {

}
