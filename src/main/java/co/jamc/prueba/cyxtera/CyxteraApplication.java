package co.jamc.prueba.cyxtera;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CyxteraApplication {

	public static void main(String[] args) {
		SpringApplication.run(CyxteraApplication.class, args);
	}

}
