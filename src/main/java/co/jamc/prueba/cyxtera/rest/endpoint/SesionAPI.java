package co.jamc.prueba.cyxtera.rest.endpoint;


import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.jamc.prueba.cyxtera.entities.Session;
import co.jamc.prueba.cyxtera.persistence.SessionRepository;
import co.jamc.prueba.cyxtera.services.AuditService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="API Sesión.",  description="API Sesión. Gestiona la creación de un espacio de trabajo para la solución de resolución de operaciones matemáticas mediante identificadores de sesión.")
@RestController
@RequestMapping("api/nuevasesion")
public class SesionAPI {
	
	private final Logger LOG = LoggerFactory.getLogger(getClass());
	
	private final SessionRepository sessionRepository;
	
	@Autowired
	private AuditService auditService;
	
	public SesionAPI(SessionRepository sessionRepository) {
		this.sessionRepository = sessionRepository;
	}

	/**
	 * Genera un espacio de trabajo (workspace) retornando el identificador de la sesión relacionada con este
	 * @param user
	 * @return identificador de sesión generada
	 */
	@ApiOperation(value = "Genera un espacio de trabajo (workspace) retornando el identificador de la sesión relacionada con este."
			+ " Se debe enviar un username (nombre de usuario), el cual quedará atado al identificador de sesión del workspace (espacio de trabajo) y se usará para efectos de auditoría. Ej: /api/nuevasesion/jmolano",
				response = String.class, httpMethod = "GET", produces="text/html")
    @GetMapping("/{user}")
    public String nuevaSession(@PathVariable String user) {
    	UUID sessionUUID = UUID.randomUUID();
    	Session session = new Session();
    	session.setSessionID(sessionUUID);
    	session.setUser(user);
    	//Almacena session en repositorio
    	sessionRepository.save(session);
    	StringBuilder respuesta = new StringBuilder("Identificador de sesión: ").append(sessionUUID.toString());
    	LOG.info(new StringBuilder(respuesta).append(". Sesión creada para usuario: " ).append(user).toString());
    	//Registra auditoría
    	auditService.saveAuditAsync(session.getSessionID().toString(), user, "api/nuevasesion", new StringBuilder("/").append(user).toString(), respuesta.toString());
    	//TODO: Opcional y pendiente validar conversión de mensaje de respuesta a formato JSON
    	return respuesta.toString();
    }
    
}
