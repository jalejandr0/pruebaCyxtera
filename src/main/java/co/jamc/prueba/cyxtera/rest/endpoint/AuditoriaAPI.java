package co.jamc.prueba.cyxtera.rest.endpoint;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import co.jamc.prueba.cyxtera.entities.Audit;
import co.jamc.prueba.cyxtera.services.AuditService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="API Auditoría.",  description="API Auditoría. Consulta de registros de auditoría almacenados en Base de Datos.")
@RestController
@RequestMapping("api/auditoria")
public class AuditoriaAPI {
	
	private final Logger LOG = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private AuditService auditService;
	
	/**
	 * Obtiene los registros de auditoría relacionados a un espacio de trabajo (workspace) cuyo identificador de sesión es enviado
	 * @param sessionid
	 * @return registros de auditoría en formato JSON
	 */
	@ApiOperation(value = "Obtiene los registros de auditoría relacionados a un espacio de trabajo (workspace) cuyo identificador de sesión es enviado. "
			+ "Se debe enviar el identificador de la sesión obtenido previamente y seguido por el operando (número entero) que se quiere agregar al espacio de trabajo (workspace/session). Ej: /api/auditoria/71d1f671-2f12-4f9f-9afd-819a4729beee",
			response = String.class, httpMethod = "GET", produces = "application/JSON")
	@GetMapping("/{sessionid}")
	public String getAudit(@PathVariable String sessionid) {
		List<Audit> auditList = auditService.getBySession(sessionid);
		String respuesta;
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		if (auditList != null && !auditList.isEmpty()) {
			String JSONObject = gson.toJson(auditList);
			LOG.info(JSONObject);
			respuesta = JSONObject;
		} else {
			respuesta = "Sesión inválida o no existe.";
			String JSONObject = gson.toJson(respuesta);
    		LOG.error(JSONObject);
    		respuesta = JSONObject;
		}
		return respuesta;
	}

}
