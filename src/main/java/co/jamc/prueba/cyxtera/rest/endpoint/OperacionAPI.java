package co.jamc.prueba.cyxtera.rest.endpoint;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.jamc.prueba.cyxtera.entities.Session;
import co.jamc.prueba.cyxtera.persistence.SessionRepository;
import co.jamc.prueba.cyxtera.services.AuditService;
import co.jamc.prueba.cyxtera.util.OperadorEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="API Operación.",  description="API Operación. Realiza y controla las ejecuciones de las operaciones solicitadas sobre el espacio de trabajo (workspace/session) relacionado.")
@RestController
@RequestMapping("api/realizaroperacion")
public class OperacionAPI {
	
	private final Logger LOG = LoggerFactory.getLogger(getClass());
	
	private final SessionRepository sessionRepository;
	
	@Autowired
	private AuditService auditService;
	
	public OperacionAPI(SessionRepository sessionRepository) {
		this.sessionRepository = sessionRepository;
	}
	
	/**
	 * Ejecuta la operación enviada por parámetro sobre cada uno de los operandos almacenados en el espacio de trabajo (workspace) relacionado con el identificador de sesión enviado.
	 * El resultado de un cálculo de cada operación se agrega como operando para el siguiente cálculo.
	 * @param sessionid
	 * @param operador
	 * @return resultado final de la ejecución de las operaciones realizadas sobre todos los operandos del espacio de trabajo
	 */
	@ApiOperation(value = "Ejecuta la operación enviada por parámetro sobre cada uno de los operandos almacenados en el espacio de trabajo (workspace) relacionado con el identificador de sesión enviado. "
			+ "Se debe enviar el identificador de la sesión obtenido previamente y seguido por el operador de la operación a realizar desaseada, dentro de las siguientes opciones: + (SUMA) | - (RESTA) | * (MULTIPLICACIÓN) | ÷ (DIVISIÓN) | ^ (POTENCIACIÓN). "
			+ "Ej: /api/realizaroperacion/71d1f671-2f12-4f9f-9afd-819a4729beee/+",
			response = String.class, httpMethod = "GET", produces = "text/html")
	@GetMapping("/{sessionid}/{operador}")
    public String realizarOperacion(@PathVariable String sessionid, @PathVariable String operador) {
		//Obtiene session de repositorio y valor de la session
		UUID id = UUID.fromString(sessionid);
    	Optional<Session> optionalSession = sessionRepository.findById(id);
    	String user;
    	String respuesta;
    	BigDecimal resultado = BigDecimal.ZERO;
    	OperadorEnum operadorEnum = OperadorEnum.getBySimbolo(operador);
    	if (operadorEnum == null) {
    		respuesta = "Error. Caracter de operación inválido. Debe ingresar un caracter de operación válido:<BR/>\n + para SUMA,<BR/>\n - para RESTA,<BR/>\n * para MULTIPLICACIÓN,<BR/>\n ÷ para DIVISIÓN,<BR/>\n ^ para POTENCIACIÓN";
			LOG.error(respuesta);
			return respuesta;
    	}    		
    	if (optionalSession != null && optionalSession.isPresent()) {
    		Session session = optionalSession.get();
    		user = session.getUser();
    		List<BigDecimal> listaOperandos = session.getListaOperandos();
    		if (listaOperandos == null || listaOperandos.isEmpty()) {
    			respuesta = "Advertencia. Lista de operandos vacía";
    			LOG.warn(respuesta);
    			return respuesta;
    		} else if (listaOperandos.size() <= 1) {
    			respuesta = "Advertencia. Insuficiente número de operandos para realizar operación matemática, debe haber mínimo 2.";
    			LOG.warn(respuesta);
    			return respuesta;
    		} else {
    			int index = 0;
		    	for (Iterator iterator = listaOperandos.iterator(); iterator.hasNext();) {
		    		BigDecimal operandoUno;
					if (index == 0) {
						resultado = (BigDecimal) iterator.next();
						++index;
						operandoUno = (BigDecimal) iterator.next();
						++index;
					} else {
						++index;
						operandoUno = (BigDecimal) iterator.next();
					}
					
					switch (operadorEnum) {
					case SUMA:
						resultado = resultado.add(operandoUno);
						//Registra auditoría
						auditService.saveAuditAsync(session.getSessionID().toString(), user, "api/realizaroperacion", new StringBuilder("Operación: ").append(operadorEnum.getOperador()).append(". Operando número: ").append(index).append(" de la lista. ").toString(), new StringBuilder("Resultado parcial: ").append(resultado.toString()).toString());
						break;
					case RESTA:
						resultado = resultado.subtract(operandoUno);
						//Registra auditoría
						auditService.saveAuditAsync(session.getSessionID().toString(), user, "api/realizaroperacion", new StringBuilder("Operación: ").append(operadorEnum.getOperador()).append(". Operando número: ").append(index).append(" de la lista. ").toString(), new StringBuilder("Resultado parcial: ").append(resultado.toString()).toString());
						break;
					case MULTIPLICACION:
						resultado = resultado.multiply(operandoUno);
						//Registra auditoría
						auditService.saveAuditAsync(session.getSessionID().toString(), user, "api/realizaroperacion", new StringBuilder("Operación: ").append(operadorEnum.getOperador()).append(". Operando número: ").append(index).append(" de la lista. ").toString(), new StringBuilder("Resultado parcial: ").append(resultado.toString()).toString());
						break;
					case DIVISION:
						resultado = resultado.divide(operandoUno);
						//Registra auditoría
						auditService.saveAuditAsync(session.getSessionID().toString(), user, "api/realizaroperacion", new StringBuilder("Operación: ").append(operadorEnum.getOperador()).append(". Operando número: ").append(index).append(" de la lista. ").toString(), new StringBuilder("Resultado parcial: ").append(resultado.toString()).toString());
						break;
					case POTENCIACION:
						resultado = resultado.pow(operandoUno.intValue());
						//Registra auditoría
				    	auditService.saveAuditAsync(session.getSessionID().toString(), user, "api/realizaroperacion", new StringBuilder("Operación: ").append(operadorEnum.getOperador()).append(". Operando número: ").append(index).append(" de la lista. ").toString(), new StringBuilder("Resultado parcial: ").append(resultado.toString()).toString());
						break;
	
					default:
						respuesta = "Operación inválida. Caracter de operación no encontrado.";
						LOG.error(respuesta);
						break;
					}
				}
		    	respuesta = new StringBuilder("Resultado final: " ).append(resultado.toString()).toString();
		    	//Registra auditoría
		    	auditService.saveAuditAsync(session.getSessionID().toString(), user, "api/realizaroperacion", new StringBuilder("Operación: ").append(operadorEnum.getOperador()).toString(), respuesta);
    		}
    	} else {
    		respuesta = "Sesión inválida o no existe.";
    		LOG.error(respuesta);
    	}
    	//TODO: Opcional y pendiente validar conversión de mensaje de respuesta a formato JSON
    	return respuesta;
	}

}
