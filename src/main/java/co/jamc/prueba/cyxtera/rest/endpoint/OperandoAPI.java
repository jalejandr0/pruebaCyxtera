package co.jamc.prueba.cyxtera.rest.endpoint;


import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.jamc.prueba.cyxtera.entities.Session;
import co.jamc.prueba.cyxtera.persistence.SessionRepository;
import co.jamc.prueba.cyxtera.services.AuditService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="API Operandos",  description="API Operandos. Gestiona los operandos que se van agregando dentro de un espacio de trabajo (workspace/sesión).")
@RestController
@RequestMapping("api/agregaroperando")
public class OperandoAPI {
	
	private final Logger LOG = LoggerFactory.getLogger(getClass());
	
	private final SessionRepository sessionRepository;
	
	@Autowired
	private AuditService auditService;
	
	public OperandoAPI(SessionRepository sessionRepository) {
		this.sessionRepository = sessionRepository;
	}

	/**
	 * Agrega operando al espacio de trabajo (workspace) creado previamente con el identificador relacionado
	 * @param sessionid
	 * @param operando
	 * @return resultado de envío de operando (número) para agregar
	 */
	@ApiOperation(value = "Adiciona un operando al espacio de trabajo. Se debe enviar el identificador de la sesión (workspace) previamente obtenido mediante el API /api/nuevasesion/{user} "
			+ "y seguido por el operando (número entero) que se quiere agregar al espacio de trabajo (workspace/session). Ej: /api/agregaroperando/71d1f671-2f12-4f9f-9afd-819a4729beee/10",
			response = String.class, httpMethod = "GET", produces = "text/html")
    @GetMapping("/{sessionid}/{operando}")
    public String agregarOperando(@PathVariable String sessionid, @PathVariable String operando) {
    	//Obtiene session de repositorio y valor de la session
    	UUID id = UUID.fromString(sessionid);
    	Optional<Session> optionalSession = sessionRepository.findById(id);
    	String user;
    	String respuesta;
    	if (optionalSession != null && optionalSession.isPresent()) {
    		Session session = optionalSession.get();
    		user = session.getUser();
    		if (StringUtils.isNumeric(operando)) {
				BigDecimal numero = new BigDecimal(operando);
				session.getListaOperandos().add(numero);
				sessionRepository.save(session);
		    	respuesta = new StringBuilder("Operando agregado: ").append(numero).toString();
		    	LOG.info(respuesta);
		    	//Registra auditoría
		    	auditService.saveAuditAsync(session.getSessionID().toString(), user, "api/agregaroperando", new StringBuilder("/").append(operando).toString(), respuesta.toString());
			} else {
				respuesta = "Error. El caracter ingresado no es válido, debe ingresar un número entero.";
				LOG.error(respuesta);
			}
    	} else {
    		respuesta = "Sesión inválida o no existe.";
    		LOG.error(respuesta);
    	}
    	//TODO: Opcional y pendiente validar conversión de mensaje de respuesta a formato JSON
        return respuesta;
    }

}
