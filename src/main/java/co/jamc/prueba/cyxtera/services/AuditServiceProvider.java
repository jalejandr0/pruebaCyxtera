package co.jamc.prueba.cyxtera.services;


import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import co.jamc.prueba.cyxtera.entities.Audit;
import co.jamc.prueba.cyxtera.persistence.AuditRepository;

@Service
public class AuditServiceProvider implements AuditService {

    @Autowired
    private AuditRepository auditRepository;

    @Override
    @Async("threadPoolTaskExecutor")
    public void saveAuditAsync(String sessionId, String user, String apiCalled, String input, String output) {
        Audit audit = new Audit(sessionId, user, apiCalled, input, output, new Date());
        auditRepository.save(audit);
    }

    @Override
    public List<Audit> getBySession(String sessionId) {
        return this.auditRepository.findBySessionId(sessionId);
    }
    
    @Override
    public List<Audit> getByUser(String user) {
        return this.auditRepository.findByUser(user);
    }

    @Override
    public List<Audit> getAll() {
        return this.auditRepository.findAll();
    }
}
