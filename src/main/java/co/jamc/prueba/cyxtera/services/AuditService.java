package co.jamc.prueba.cyxtera.services;

import co.jamc.prueba.cyxtera.entities.Audit;

import java.util.List;

public interface AuditService {


    public void saveAuditAsync(String sessionId, String user, String apiCalled, String input, String output);

    public List<Audit> getBySession(String sessionId);
    
    public List<Audit> getByUser(String user);

    public List<Audit> getAll();

}
