package co.jamc.prueba.cyxtera.util;

public enum OperadorEnum {
	
	SUMA ("SUMA", "+"),
	RESTA ("RESTA", "-"),
	MULTIPLICACION ("MULTIPLICACION", "*"),
	DIVISION ("DIVISION", "÷"),
	POTENCIACION ("POTENCIACION", "^");
	
	private String operador;
	
	private String simbolo;

	private OperadorEnum(String operador, String simbolo) {
		this.operador = operador;
		this.simbolo = simbolo;
	}

	public String getOperador() {
		return operador;
	}

	public void setOperador(String operador) {
		this.operador = operador;
	}

	public String getSimbolo() {
		return simbolo;
	}

	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}
	
	public static OperadorEnum getBySimbolo(String simbolo) {
		OperadorEnum operador = null;
		for (OperadorEnum operadorEnum : OperadorEnum.values()) { 
		    if (operadorEnum.getSimbolo().equalsIgnoreCase(simbolo)) {
		    	operador = operadorEnum;
		    	break;
		    }
		}
		return operador;
	}

}
